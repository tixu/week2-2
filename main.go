package main

import "github.com/pkg/errors"



// Right modelize a permission on a file
type Right int

// Application is a representation of an application
type Application struct {
	domain string
	name   string
}

//Ressource is an entity that needs to be protected
type Ressource string

type aclcontainer map[Ressource][]Application

type aclManager struct {
	Read   aclcontainer
	Write  aclcontainer
	Delete aclcontainer
}

// ACLChecker is the behavior that needs to be implemented by the authorizer
type ACLChecker interface {
	HasRight(Ressource, Application, Right) (bool, error)
}

//HasRight checks if the res Resource can be access by the app with the right.
func (acl *aclManager) HasRight(res Ressource, app Application, right Right) (bool, error) {
	switch right {
	case Read:
		applications, ok := acl.Read[res]
		if ok {
			ok = false
			for i := range applications {
				if applications[i] == app {
					ok = true
					break
				}
			}
		}
		return ok, nil
	case Write:
		applications, ok := acl.Write[res]
		if ok {
			ok = false
			for i := range applications {
				if applications[i] == app {
					ok = true
					break
				}
			}
		}
		return ok, nil
	case Delete:
		_, ok := acl.Delete[res]
		applications, ok := acl.Read[res]
		if ok {
			ok = false
			for i := range applications {
				if applications[i] == app {
					ok = true
					break
				}
			}
		}
		return ok, nil
	default:
		return false, errors.New("unsupported right")
	}
}

// Read correspond to read access rigth
const (
	Read Right = 1 + iota
	Write
	Delete
)

func main() {

}

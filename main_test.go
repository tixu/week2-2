package main

import "testing"

func TestMain(t *testing.T) {
	resource1 := Ressource("File1")
	resource2 := Ressource("File2")
	ITR := Application{
		domain: "BB-Network",
		name:   "ITR",
	}
	ITC := Application{
		domain: "BB-Network",
		name:   "ITC",
	}
	manager := aclManager{
		Read:   make(map[Ressource][]Application),
		Write:  make(map[Ressource][]Application),
		Delete: make(map[Ressource][]Application),
	}

	manager.Read[resource1] = append(manager.Read[resource1], ITR)
	manager.Read[resource2] = append(manager.Read[resource2], ITC)
	manager.Write[resource1] = append(manager.Write[resource1], ITR)
	manager.Delete[resource2] = append(manager.Delete[resource2], ITC)

	_, err := manager.HasRight(resource1, ITC, 8)
	if err == nil {
		t.Fatal("exepecting an execption")
	}
	ok, _ := manager.HasRight(resource2, ITC, Delete)
	if !ok {
		t.Fatal("exepecting an true")
	}
}

// A naive approacje
func Test_aclManager_HasRight(t *testing.T) {
	resource1 := Ressource("File1")
	resource2 := Ressource("File2")
	ITR := Application{
		domain: "BB-Network",
		name:   "ITR",
	}
	AFC := Application{
		domain: "BB-Network",
		name:   "AFC",
	}
	ITC := Application{
		domain: "BB-Network",
		name:   "ITC",
	}
	manager := aclManager{
		Read:   make(map[Ressource][]Application),
		Write:  make(map[Ressource][]Application),
		Delete: make(map[Ressource][]Application),
	}

	manager.Read[resource1] = append(manager.Read[resource1], ITR)
	manager.Read[resource2] = append(manager.Read[resource2], ITC)
	manager.Write[resource1] = append(manager.Write[resource1], ITR)
	manager.Delete[resource2] = append(manager.Delete[resource2], ITC)

	type args struct {
		res   Ressource
		app   Application
		right Right
	}

	tests := []struct {
		name    string
		acl     *aclManager
		args    args
		want    bool
		wantErr bool
	}{
		{
			name:    "check read access",
			acl:     &manager,
			args:    args{res: resource1, app: ITR, right: Read},
			want:    true,
			wantErr: false,
		},
		{
			name:    "bad right code",
			acl:     &manager,
			args:    args{res: resource1, app: ITR, right: 8},
			want:    false,
			wantErr: true,
		},
		{
			name:    "no right to access the resources",
			acl:     &manager,
			args:    args{res: resource1, app: AFC, right: Read},
			want:    false,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		got, err := tt.acl.HasRight(tt.args.res, tt.args.app, tt.args.right)
		if (err != nil) != tt.wantErr {
			t.Errorf("%q. aclManager.HasRight() error = %v, wantErr %v", tt.name, err, tt.wantErr)
			continue
		}
		if got != tt.want {
			t.Errorf("%q. aclManager.HasRight() = %v, want %v", tt.name, got, tt.want)
		}
	}
}
